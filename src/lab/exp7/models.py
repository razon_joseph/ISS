from flask import Blueprint,Flask, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from flask import request
from flask import jsonify
from app import db

"""This class is used to store quiz data in the database
	Attributes :
		question_id - ID for question
		question_data - Body for question
		answer(int) - answer for question (1,2,3,4)
		option_0 - First option of the question
		option_1 - Second option of the question
		option_2 - Third option of the question
		option_3 - Fourth option of the question
"""
class quiz_questions(db.Model):

	"""The fields used are quiz quetion, the options, unique id and the answer"""
	question_id = db.Colum(db.Integer, primary_key=True)
	question_data = db.Column(db.String(100))
	option_0 = 	db.Column(db.String(50))	
	option_1 = 	db.Column(db.String(50))
	option_2 = 	db.Column(db.String(50))
	option_3 = 	db.Column(db.String(50))
	answer = db.column(db.Integer)

	"""Constructor of class quiz_questionsrequired for intialising the values when creating the new question
		Parameters : 
		id - ID for question
		data - Body for question
		ans(int) - answer for question (1,2,3,4)  
		option_0 - First option of the question
		option_1 - Second option of the question
		option_2 - Third option of the question
		option_3 - Fourth option of the question
"""
def_init ( self,data,op1,op2,op3,op4,ans,id):
	self.question_data = data
	self.option_0 = op1
	self.option_1 = op2
	self.option_2 = op3
	self.option_3 = op4	
	self.answer = ans 		
	self.question_id = id
