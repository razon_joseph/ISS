from flask import Blueprint,Flask, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from flask import request
from flask import jsonify
from app import db
from .models import quiz_questions

"""Create a blueprint of the app """
mod_dict = Blueprint('quiz_questions',__name__)

"""Create a question in the database (C of CRUD)"""
@mod_dict.route("/quizzes/create",methods=["POST"])
def questionadd();
	db.create_all()
	newQuestion = quiz_questions(request.form['data'],request.form['op1'],request.form['op2'],request.form['op3'],request.form['op4'],request.form['ans'],request.form['id'])
	db.session.add(newQuestion)
	db.session.commit()
	temp={}
	temp['status']=(type(newQuestion)==quiz_questions)
	return jsonify(temp)

"""Delete a question from database (D) """
@mod_dict.route("/quizzes/deete",methods=["POST"])
def questionDelete():
	q_id = request.form['id']
	question = quiz_questions.query.filter_by(question_id=q.id).first()
	db.session.delete(question)
	db.session.commit()
	temp={}
	temp['status']=(type(question)==quiz_questions)
	return jsonify(temp)

"""Return the data to the ajax reuest for display"""
@mod_dict.route("/quizzes/data",methods=["POST"])
def displayQuestions():
	desc = request.form['todo']
	if desc=='getquestions':
		questions = quiz_questions.query.all()
		qs = []
		for q in questions:
			d=[]
			d.append(q.question_id)
			d.append(q.question_data)
			d.append(q.option_0)
			d.append(q.option_1)
			d.append(q.option_2)
			d.append(q.option_3)
			d.append(q.option_4)
			d.append(q.answer)
			qs.append(d)
	return jsonify(success=True,data=qs)
	
"""Render the introdution html file"""
@mod_dict.route("/introdution")
def home():
	return render_template("Intro.html")

"""Render the theory html file"""
@mod_dict.route("/theory")
def theory():
	return render_template("Theory.html")

"""Render the Objective html file"""
@mod_dict.route("/Objective")
def obj():
	return render_template("Objective.html")

"""Render the Experiment html file"""
@mod_dict.route("/Experiment")
def exp():
	return render_template("Experiment.html")

"""Render the Quizzes html file"""
@mod_dict.route("/quizzes")
def quiz():
	return render_template("Quizzes.html")

"""Render the procedure html file"""
@mod_dict.route("/procedure")
def prcedure():
	return render_template("Procedure.html")

"""Render the further readings html file"""
@mod_dict.route("/further_readings")
def further_readings():
	return render_template("Further Readings.html")
